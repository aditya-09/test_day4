package org.example;

public class Getter extends Setter {

    public Integer getId(){
        return this.newId;
    }

    public String getName() {
        return this.newName;
    }

    public String getPosition(){
        return newPosition;
    }

    public Integer getSalary(){
        if (getPosition() == "Karyawan"){
            this.salary = getId() * 3000000;
        }else {
            this.salary = getId() * 10000000;
        }
        return this.salary;
    }
    
}
