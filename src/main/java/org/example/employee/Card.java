package org.example.employee;

public class Card {
    private int[] ID = {01, 02, 03, 04, 05};
    private String[] Name = {"Kurniawan", "Putra", "Adit", "Ate", "Dit"};
    private String[] position = {"Karyawan", "Karyawan", "Karyawan", "Manager", "Manager"};
    private String[][] card = {
            {"01","Kurniawan","Karyawan"},
            {"02","Putra","Karyawan"},
            {"03", "Adit", "Karyawan"},
            {"04", "Ate","Manager"},
            {"05", "Dit","Manager"}};

    public int[] getId(){
        return ID;
    }

    public String[] getName(){
        return Name;
    }

    public String[] getPosition(){
        return position;
    }

    public String[][] getCard(){
        return card;
    }

}
