package org.example;

public class Setter {
    protected Integer newId;
    protected String newName;
    protected String newPosition;
    protected int salary;

    public void setId(int ID){
        this.newId = ID;
    }

    public void setName(String name) {
        this.newName = name;
    }

    public void setPosition(String position){
        this.newPosition = position;
    }
}
